
:: MIT License
:: 
:: Copyright (c) 2018 Luca Matteu
:: 
:: Permission is hereby granted, free of charge, to any person obtaining a copy
:: of this software and associated documentation files (the "Software"), to deal
:: in the Software without restriction, including without limitation the rights
:: to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
:: copies of the Software, and to permit persons to whom the Software is
:: furnished to do so, subject to the following conditions:
:: 
:: The above copyright notice and this permission notice shall be included in all
:: copies or substantial portions of the Software.
:: 
:: THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
:: IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
:: FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
:: AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
:: LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
:: OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
:: SOFTWARE.
  

setlocal enableextensions enabledelayedexpansion

:: IMPORTANT NOTICE
:: (PLEASE TAKE A MOMENT TO READ IT)
::
:: The present script is intended as a proxy interface to run the (target) zip-packaging tool,
:: and its responsibilities are meant to be pretty limited to: provide input normalization, if needed;
:: specifically launch on Windows what is due.
::
:: The fact that this script, in its current form, expects the zip-tool parameters' values to be passed as input arguments,
:: serves two (opposite) purposes at the same time:
::   1. being easily adaptable to act as a direct launch interface, so that you just need to replace the arguments notation (e.g. '1%')
::      with the concrete, actual values you need the parameters to be filled in with, and you are done.
::   2. enforcing on you a good practice, which is separating the parameters-population logics from the OS-specific data-normalization logics needed for
::      the call to the zipping utility.
::
:: Which path to walk is up to you, but the strategy you should really prefer is the one described at point number 2.
::
:: In fact, in the long term, in no way is this script intended as the correct place to directly populate the tool-parameters' values.
:: On the contrary, you are strongly encouraged to:
::   ▪ extend the input-arguments form also to those tool-parameters that currently have not (e.g. 'outputZipDirectory' and 'emptyZipDirBeforeStart')
::   ▪ let this script follow the "input-arguments pattern", and write elsewhere the code you need to valorize the parameters and to pass them to this script.
::     that way, when calling the zip-packaging tool, such a structure forces you, as already said, not to mix up the parameters-population logics
::     with the OS-specific data-normalization logics needed for the call
::     (i.e.: leave here the OS-specific logics needed for the call,
::            and you will be able to implement, upstream, the parameters-population logics either:
::              • via another script, by reading the tool-parameters' values as input from the user
::              • via another script, by reading the tool-parameters' values from a properties file
::              • via a third parties application, by having it populate the tool parameters for you based on certain rules, and having the tool, not your code,
::                pass them to this script).
::
:: All the above being said, the required parameters are documented inline below:
:: you can use such documentation as a guidance to write, if needed, your own parameters-population code.


:: ******************************************************************************************************************************************************************
:: LAUNCH PARAMETERS

:: 									WARNING!!! 
:: ON WINDOWS, IF YOU WANT TO INPUT COMMIT IDs CONTAINING CMD SPECIAL CHARACTERS (e.g.: HEAD^1),
:: YOU HAVE TO PROVIDE THEM ESCAPED BY ^ (e.g. HEAD^^1)

:: Full path to the local directory that contains the zip packaging tool to launch - i.e. zipPackager.sh.
:: Just the absolute path of the directory, without any file name.
:: 			MANDATORY.
set packagingToolDirectory=%1
set packagingToolDirectory.inUnixForm=%packagingToolDirectory:\=/%

:: Full path to the local directory containing the GIT repository to create the zip-package for.
:: It is the parent folder of the hidden .git directory.
:: 			MANDATORY.
set gitRepoPath=%2
set gitRepoPath.inUnixForm=%gitRepoPath:\=/%

:: Full path to the local directory in which the output zip archive must be produced.
:: 			MANDATORY.
:: HERE, A DEFAULT VALUE IS DEFINED.
set outputZipDirectory=%packagingToolDirectory%/output/
set outputZipDirectory.inUnixForm=%outputZipDirectory:\=/%

:: Boolean value. If true, the tool first deletes any zip file in the zip output directory, and then starts processing.
:: Intended to clean up the last-run files, in order to avoid visual pollution in the zip output directory: one run, one zip.
:: Use with caution: the safer option is to turn this setting on only in case you use, for zip generation, an exclusively-dedicated directory.
:: 			MANDATORY.
:: HERE, A DEFAULT VALUE IS DEFINED.
set emptyZipDirBeforeStart=false

:: ID of the commit-object from which to start the diff. Diff starts from this commit-object, and EXCLUDES it.
:: Any valid commit-ish identifier is supported (tags, SHA1 commit hashes, etc.).
:: 			MANDATORY.
set commitRangeStart="%3"

:: ID of the commit-object at which to stop the diff. Diff ends to this commit-oject, and INCLUDES IT.
:: Any valid commit-ish identifier is supported (tags, SHA1 commit hashes, etc.).
:: 			MANDATORY.
set commitRangeEnd="%4"

:: ******************************************************************************************************************************************************************

:: TOOL LAUNCH
unix2dos "%packagingToolDirectory.inUnixForm%/zipPackager.sh"		
"%packagingToolDirectory.inUnixForm%/zipPackager.sh" "%gitRepoPath.inUnixForm%" "%outputZipDirectory.inUnixForm%" %commitRangeStart% %commitRangeEnd% %emptyZipDirBeforeStart%