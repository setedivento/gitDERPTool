<h2>
<details>
<summary>GITDERPtool</summary>
&nbsp;&nbsp;&nbsp;&nbsp;<sub>A.K.A. GIT Differences Effortless Release-Packaging Tool</sub>
</details>
</h2>
A bash tool to build Deploy Packages from local GIT repositories, based on changes intercurring between two commits.  
<br/><br/> 
   
If you have a GIT repository, cloned locally from a certain remote, GITDERPtool can produce a zip archive out of the currently checked out branch, 
and the zip will contain all the files, say, from commit A to commit B as they were in the given commit-range, including their original directory structure.  
  
  
Still not supported at the moment:  
&nbsp;&nbsp;\- GIT repositories with symbolic links resources inside  
&nbsp;&nbsp;\- automatic cloning of the target repository from a url.  

### Minimum System Requirements  
 * <pre>a working GIT Bash.  
   Windows: install GIT for Windows from here: <a href="https://git-scm.com/download/win">https://git-scm.com/download/win</a>  
   Linux: install GIT</pre>
     
 * <pre>from the GIT Bash, access to the 'dos2unix'/'unix2dos' utilities.  
   Windows: either install a porting of the utilities themselves,  
            and add their location to the 'Path' system environment variable,  
            or install GOW from here: <a href="https://github.com/bmatzelle/gow/releases">https://github.com/bmatzelle/gow/releases</a>  
   Linux: istall unix2dos</pre>
     
 * <pre>from the GIT Bash, access to the 'ziptool' and 'zipmerge' utilities.  
   Windows: said utilities should already be shipped along with GIT for Windows, but as for 'zipmerge',  
   it has to be checked that it correctly works; please do so by issuing this command  
   in a terminal window: '<i>zipmerge -h</i>'  
   (installation of the additional 'libp11-kit-0.dll' library could be required).  
   Linux: install zipmerge</pre>
      
 * <pre>for Unit Testing: generally speaking, first of all please have a look at bats documentation here:  
   <a href="https://github.com/bats-core/bats-core">https://github.com/bats-core/bats-core</a>  
   Windows: no additional requirement  
   Linux: install bats using <a href="https://github.com/bats-core/bats-core#installing-bats-from-source">this</a> guideline</pre>  

### Usage  
You can notice there are two main scripts, at root level:
  1. launch.onLinux.sh  
  2. launch.onWindows.bat  

Both serve as the main launch interface, each one for a specific OS to pick.  
  
Inside the scripts, you will find the parameters needed to configure GITDERPtool (target repository, zip output path, and so on...).  
Each parameter and its purpose is well documented inside the scripts: please refer to them for more details.  

<i>WARNING</i>: on Windows, if you want to input commit ids containing cmd special characters (e.g.: HEAD^1),  
you have to provide them escaped by ^ (e.g. HEAD^^1).   

### Linux advices  
The tool has been tested on Ubuntu 18.04.  
.sh scripts are to be launched via bash, e.g. <i>bash launchTests.onLinux.sh</i>.  
  
Besides, a GIT-credentials storage system is recommended. For example, on Ubuntu, you can use gnome-keyring, installing it as follows:  
```bash
sudo apt-get install libgnome-keyring-dev
sudo make --directory /usr/share/doc/git/contrib/credential/gnome-keyring
git config --global credential.helper /usr/share/doc/git/contrib/credential/gnome-keyring/git-credential-gnome-keyring
```
