: '
  MIT License
  
  Copyright (c) 2018 Luca Matteu
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
'

#!/usr/bin/bash

# ******************************* FUNCTIONS DECLARATION *******************************
isWindows() { [[ -n "$WINDIR" ]]; }

getTime() {
	# need to use cmd on Windows because the 'date' ported command does not (correctly) handle timezones.
	# e.g. daylight savings are not considered, maybe because the TZ env variable is not available,
	# and/or Windows does not have any 'usr/share/zoneinfo' system to store timezones' infos
    if isWindows; then
		# 'tr' is needed to replace spaces with zeroes, like for example when, for the hour, you have ' 0' instead of '00' (at midnight)
        cmd.exe "/c echo %DATE:~6,4%%DATE:~3,2%%DATE:~0,2%_%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%" | head -c 15 | tr " " 0 ;
    else
        date +%Y%m%d_%H%M%S ;
    fi
}
# *************************************************************************************

# ******************************* VARIABLES DECLARATION *******************************
gitRepoPath="$1"
zipFilePath="$2"
commitRangeStart="$3"
commitRangeEnd="$4"
emptyZipFileDirBeforeStart="$5"

deletedSourcesFileName=filesToDelete.txt # file name without spaces
zippingErrorsFileName=zippingErrors.txt # file name without spaces
rightNow=$(getTime)
# *************************************************************************************

# ******************************** PROCESS ********************************************

# CREATES ZIP DIRECTORY IF NEEDED
mkdir -p "$zipFilePath"

# EMPTIES ZIP DESTINATION DIRECTORY IF REQUIRED
cd "$zipFilePath"
if $emptyZipFileDirBeforeStart; then
   rm -rf *.zip $deletedSourcesFileName $zippingErrorsFileName ;
fi

# SHIFTS TO GIT REPO DIR
cd "$gitRepoPath"
echo Changed Directory to: "$gitRepoPath".

# BUILDS ZIP FILE NAME
zipFileName=$(basename "$gitRepoPath")_$(git rev-parse --abbrev-ref HEAD)_$rightNow

# CALCULATES DIFFERENCES
differences=$(git diff --name-only --diff-filter=ACMRT "$commitRangeStart" "$commitRangeEnd")

# PROCESSES DIFFERENCES IN CHUNKS, GENERATING A ZIP EVERY 100 ITEMS
readarray diffs <<< "$differences"
diffsCount=${#diffs[*]}
for ((diffIndex=0; diffIndex<$diffsCount; diffIndex+=100)); do
   diffsChunkWithQuotedFiles="$(echo -e "${diffs[@]:diffIndex:100}" | sed -E 's/^[[:space:]]*(.+)$/"\1"/g')"
   echo "$diffsChunkWithQuotedFiles" | xargs git archive -o "$zipFilePath"/"$zipFileName"_$diffIndex".zip" "$commitRangeEnd"
done

# MERGES FINAL ZIP FROM ALL CREATED ONES SO FAR
allGeneratedZips=$(find "$zipFilePath"/ -name "*.zip" -exec echo '"{}"' \;)
echo $allGeneratedZips | xargs zipmerge "$zipFilePath"/"$zipFileName"".zipTmp"

# CLEANS UP EVERYTHING
# on Windows, 'zipmerge' generates a zip that is not readable by 'ls' nor 'find'.
# need to use 'cp' + 'rm' because it's the only way to make the zip show up again.
# warning: 'cp' works, 'mv' doesn't; 'rm' with exact file name works, 'rm' with '*.zipTmp' pattern doesn't.
rm -rf "$zipFilePath"/*.zip
cp "$zipFilePath"/"$zipFileName"".zipTmp" "$zipFilePath"/"$zipFileName"".zip"
rm -rf "$zipFilePath"/"$zipFileName"".zipTmp"
echo Executed Zipping Operations.

# GENERATES DELETED FILES LIST (INCLUDES RENAMED FILES AND DIRECTORIES)
commitsSHA1s=$(git rev-list --reverse --boundary $commitRangeStart..$commitRangeEnd | tr -d '-')
readarray sha1s <<< "$commitsSHA1s"
sha1sCount=${#sha1s[*]}
# 'git diff --find-renames' is not an option here, because it does not work correctly across multiple commits
for ((commitIndex=0; commitIndex<(sha1sCount - 1); commitIndex+=1)); do
   singleCommitRange=$(echo -n "${sha1s[@]:commitIndex:2}")
   # simple 'git diff' (vs 'git diff-tree') seems not to work when it has to list deleted files
   commitDeletedFiles=$(git diff-tree -r --no-commit-id --name-only --diff-filter=D $singleCommitRange)
   if [ ! -z "$commitDeletedFiles" ] ; then
	   deletedFiles+="${commitDeletedFiles}\n" ;
   fi   
done
if [ ! -z "$deletedFiles" ] ; then
    echo -en "$deletedFiles" > "$zipFilePath"$deletedSourcesFileName ;
fi
echo Processed Deleted Files List.

# ...

# FINAL MESSAGE
echo Everything "done".
#read -rsp $'Press Enter to Continue...\n'

# *************************************************************************************
