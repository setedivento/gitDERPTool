: '
  MIT License
  
  Copyright (c) 2018 Luca Matteu
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
'

#!/usr/bin/bash

printLegend() {
  # ANSI escape codes: https://en.wikipedia.org/wiki/ANSI_escape_code
  local NOCOLOR='\033[0m'
  local WHITE='\033[1;37m'
  local LIGHTGRAY='\033[0;37m'
  local RED='\033[0;31m'
  
  echo -e "${WHITE}LEGEND"
  echo -e "  - File operations:"
  echo -e "      A. a file is renamed"
  echo -e "      B. a directory is renamed"
  echo -e "      C. a file is deleted"
  echo -e "      D. a directory is deleted"
  echo -e "      E. a directory is moved inside another"
  echo -e "      F. a file is moved inside another directory, keeping the old directory${NOCOLOR}"
}

echo
echo Launching Tests...
echo
printLegend # this is printed here because, unfortunately, it doesn't work correctly if printed inside '.bats' file. should belong to '.bats' file, though.
echo

toolDirectory="$1" "$2"bats testSuite.bats
