: '
  MIT License
  
  Copyright (c) 2018 Luca Matteu
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
'

# SEE HERE FOR DOCUMENTATION
# https://github.com/bats-core/bats-core
# https://github.com/bats-core/bats-core/wiki/Bats-Evaluation-Process

# USE THIS FOR DEBUGGING
#echo "# $someVariable" >&3    ('#' is needed to create TAP compliant messages)

# EXAMPLES
: '
#@test "Example test" {
  run git --version
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "git version 2.19.0.windows.1" ]
}
#unzip "$zipPathname" -d "$zipFilePath"
'
# ----------------------------------------------------------------------------

setup() {
	# variable $toolDirectory is received as an external argument
	runningDirectory=$PWD
	gitRepoPath="$PWD/testRepo/"
	gitRepoPathWithSpaces="$PWD/testRepo withSpaces/"
	zipFilePath="$runningDirectory/output/"
	emptyZipFileDirBeforeStart=true
	
	if [ "$BATS_TEST_NUMBER" -eq 1 ] ; then
		cd "$toolDirectory" ;
		local firstRemoteName=$(git remote show | head -n 1) ;
		local remoteUrl=$(git config --get remote."$firstRemoteName".url) ;
		if [ ! -d "$gitRepoPath" ] ; then
			git clone -b commitSet_1 "$remoteUrl" "$gitRepoPath" ;
		fi
		if [ ! -d "$gitRepoPathWithSpaces" ] ; then
			git clone -b commitSet_2 "$remoteUrl" "$gitRepoPathWithSpaces" ;
		fi
	fi
	
}

# ****************************************************************************


@test "1. Zip is produced correctly even if GIT Repo path contains spaces" {
  local commitRangeStart="1b10a942b5ce82bec36b0c34a510e32d84d43eb2"
  local commitRangeEnd="fde666f89c56741f85ca1cd2e88839f183406637"
  
  run "$toolDirectory"/zipPackager.sh "$gitRepoPathWithSpaces" "$zipFilePath" "$commitRangeStart" "$commitRangeEnd" "$emptyZipFileDirBeforeStart"
  
  local zipPathname=$(find "$zipFilePath" -name "testRepo withSpaces*.zip")
  echo "# Zip Path: $zipPathname"   # this is printed only in case of error
  
  local zipContent=$(unzip -l "$zipPathname")
  echo "# Zip Content: $zipContent"   # this is printed only in case of error
  
  [ -n "$zipPathname" ]
  [[ $zipContent = *'unaffectionateness stuff/'* ]]
  [[ $zipContent = *'love file.txt'* ]]
  [[ $zipContent = *'hatred file.txt'* ]]
  [[ $zipContent != *'undecidedFile.txt'* ]]
}


@test "2. Zip is produced correctly even if Zip destination path contains spaces" {
  local commitRangeStart="6dcbeefb7d963bb0b8ccd1ca37f3250ce514c8f3"
  local commitRangeEnd="6862966fbf52297ba50a46838e046ffe913fa045"
  
  local zipPath="$zipFilePath/output with spaces/"
  
  run "$toolDirectory"/zipPackager.sh "$gitRepoPath" "$zipPath" "$commitRangeStart" "$commitRangeEnd" "$emptyZipFileDirBeforeStart"
  
  local zipPathname=$(find "$zipPath" -name "*.zip")
  echo "# Zip Path: $zipPathname"   # this is printed only in case of error
  
  local zipContent=$(unzip -l "$zipPathname")
  echo "# Zip Content: $zipContent"   # this is printed only in case of error
  
  [ -n "$zipPathname" ]
  [[ $zipContent = *'binary/name with spaces.pdf'* ]]
  [[ $zipContent = *'dummy/emptyFileToCommit1.txt'* ]]
  [[ $zipContent = *'moved/originallyInAnotherPlace/testFile.txt'* ]]
  [[ $zipContent != *'dummyFile.txt'* ]]
}


@test "3. Source files/directories with spaces in their names are correctly included in zip" {
  skip "THIS CHECK IS ALREADY INCLUDED IN TEST NUMBER 1."
}


@test "4. When file operations A,B,C,D,E,F are performed across a range of commits, so that <1 operation = 1 commit>, all involved resources are correctly marked as to delete" {
  local commitRangeStart="3b041a382d5952692cea61c98d1ff363f7b459e8"
  local commitRangeEnd="977366b1d50ed903179948301eace3e3725913a6"
  
  run "$toolDirectory"/zipPackager.sh "$gitRepoPath" "$zipFilePath" "$commitRangeStart" "$commitRangeEnd" "$emptyZipFileDirBeforeStart"
  
  local filesCount=$(wc -l < "$zipFilePath"/filesToDelete.txt)

  echo "# Files to delete: $filesCount. Should be: 7"   # this is printed only in case of error
  
  [ "$filesCount" -eq 7 ]
}


@test "5. When file operations A,B,C,D,E,F are performed within a single commit, all involved resources are correctly marked as to delete" {
  local commitRangeStart="65c3e85777b22b5ac3d45f7ce41fbfa353dea1ff"
  local commitRangeEnd="5482531d58333d9f3478875cd018cc2ca1392467"
  
  run "$toolDirectory"/zipPackager.sh "$gitRepoPath" "$zipFilePath" "$commitRangeStart" "$commitRangeEnd" "$emptyZipFileDirBeforeStart"
  
  local filesCount=$(wc -l < "$zipFilePath"/filesToDelete.txt)

  echo "# Files to delete: $filesCount. Should be: 6"   # this is printed only in case of error
  
  [ "$filesCount" -eq 6 ]
}


@test "6. In case of a 2000-files commit, ZIP contains all 2000 files" {
  local commitRangeStart="97500182bb0c95d4b92dc993da2eaf5b95c8258a"
  local commitRangeEnd="3b041a382d5952692cea61c98d1ff363f7b459e8"
  
  run "$toolDirectory"/zipPackager.sh "$gitRepoPath" "$zipFilePath" "$commitRangeStart" "$commitRangeEnd" "$emptyZipFileDirBeforeStart"
  
  local zipPathname=$(find "$zipFilePath" -name "*.zip")
  local filesCount=$(unzip -l "$zipPathname" | grep '\.txt$' | wc -l)
  
  echo "# Files count is: $filesCount. Should be: 2000"   # this is printed only in case of error
  
  [ "$filesCount" -eq 2000 ]
}

@test "7. Zip is produced correctly using tags as commit objects, even with strange characters in tags' names" {
  local commitRangeStart="start_tag"
  local commitRangeEnd="end/tag"
  
  run "$toolDirectory"/zipPackager.sh "$gitRepoPath" "$zipFilePath" "$commitRangeStart" "$commitRangeEnd" "$emptyZipFileDirBeforeStart"
  
  local zipPathname=$(find "$zipFilePath" -name "*.zip")
  local filesCount=$(unzip -l "$zipPathname" | grep '\.\(txt\|pdf\)$' | wc -l)
  
  echo "# Files count is: $filesCount. Should be: 4"   # this is printed only in case of error
  
  [ "$filesCount" -eq 4 ]
}


# ****************************************************************************

teardown() {
	# just an empty instruction
	:
	
	if [ "$BATS_TEST_NUMBER" -eq 2 ] ; then
		cd "$zipFilePath/output with spaces" ;
	    rm -rf * ;
		cd .. ;
		rm -rf * ;
	elif [ "$BATS_TEST_NUMBER" -eq 7 ] ; then
	    cd "$zipFilePath" ;
		rm -rf * ;
		cd .. ;
		rm -rf output ;
	fi
}

